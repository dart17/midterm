import 'package:midterm/midterm.dart' as midterm;
import 'dart:io';

void main() {
  print("Enter Math expression: ");
  String? expression = stdin.readLineSync();
  List<String> result = tokenizingString(expression!);
  print(result);
}

List<String> tokenizingString(String expression) {
  List<String> token = <String>[];
  String buff = "";
  bool store = false;
  for (int i = 0; i < expression.length; i++) {
    if (buff.isNotEmpty) {
      if (!(expression[i] == " ")) {
        if ((expression[i] == "+") || (expression[i] == "-")) {
          if ((buff[buff.length - 1] == ")") ||
              (isNumeric(buff[buff.length - 1]))) {
            if (token.isEmpty) {
              token.add(buff);
            } else if (token[token.length - 1] != buff) {
              token.add(buff);
            }

            if (token.isEmpty) {
              token.add(expression[i]);
            } else if (token[token.length - 1] != expression[i]) {
              token.add(expression[i]);
            }
            store = true;
          } else {
            buff += expression[i];
            continue;
          }
        } else if ((expression[i] == "*") ||
            (expression[i] == "/") ||
            (expression[i] == "^") ||
            (expression[i] == "(") ||
            (expression[i] == ")")) {
          if (token.isEmpty) {
            token.add(buff);
          } else if (token[token.length - 1] != buff) {
            token.add(buff);
          }
        } else {
          if ((buff == "*") ||
              (buff == "/") ||
              (buff == "(") ||
              (buff == ")") ||
              (buff == "^") ||
              (buff == "+") ||
              (buff == "-")) {
            if ((buff == "+") || (buff == "-")) {
              if (store) {
                buff = expression[i];
              } else {
                buff += expression[i];
              }
            } else {
              buff = expression[i];
            }
          } else {
            buff += expression[i];
          }

          if (i == expression.length - 1) {
            token.add(buff);
          }
          continue;
        }
      }
    }

    if (expression[i] != " ") {
      buff = expression[i];
      if ((buff == "*") ||
          (buff == "/") ||
          (buff == "(") ||
          (buff == ")") ||
          (buff == "^")) {
        if (token.isEmpty) {
          token.add(buff);
        } else if (token[token.length - 1] != buff) {
          token.add(buff);
        }
      }
    }
  }

  return token;
}

bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return double.tryParse(s) != null;
}

