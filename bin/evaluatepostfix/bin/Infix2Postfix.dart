import 'dart:io';
import 'Tokenizing.dart';

void main() {
  print("Enter Math expression: ");
  String? expression = stdin.readLineSync();
  List<String> result = infix2postfix(tokenizingString(expression!));
  print(result);
}

List<String> infix2postfix(List<String> tokens) {
  List<String> operators = <String>[];
  List<String> postfix = <String>[];
  for (String token in tokens) {
    try {
      int num = int.parse(token);
      postfix.add(token);
    } catch (e) {
      if ((token == "+") ||
          (token == "-") ||
          (token == "*") ||
          (token == "/") ||
          (token == "^")) {
        while ((operators.isNotEmpty) &&
            (operators[operators.length - 1] != "(") &&
            (precedenceLevel(token) <
                precedenceLevel(operators[operators.length - 1]))) {
          postfix.add(operators[operators.length - 1]);
          operators.remove(operators.length - 1);
        }
        operators.add(token);
      } else if (token == "(") {
        operators.add(token);
      } else if (token == ")") {
        while ((operators[operators.length - 1] != "(")) {
          postfix.add(operators[operators.length - 1]);
          operators.remove(operators.length - 1);
        }
        operators.remove("(");
      }
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators[operators.length - 1]);
    operators.remove(operators.length - 1);
  }
  return postfix;
}

int precedenceLevel(String op) {
  switch (op) {
    case "+":
    case "-":
      return 0;
    case "*":
    case "/":
      return 1;
    case "^":
      return 2;
    default:
      throw FormatException("Operator unknown: " + op);
  }
}