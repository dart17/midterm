import 'package:evaluatepostfix/evaluatepostfix.dart' as evaluatepostfix;
import 'dart:io';
import 'dart:math';
import 'Infix2Postfix.dart';
import 'Tokenizing.dart';

void main() {
  print("Enter Math expression: ");
  String? expression = stdin.readLineSync();
  num result = evaluate(infix2postfix(tokenizingString(expression!)));
  print(result);
}

num evaluate(List<String> postfix) {
  List<num> values = <num>[];
  num firstValue;
  for (String token in postfix) {
    try {
      double num = double.parse(token);
      values.add(num);
    } catch (e) {
      num right = values.removeLast();
      num left = values.removeLast();
      num result = 0;
      if (token == "+") {
        result = right + left;
      } else if (token == "-") {
        result = left - right;
      } else if (token == "*") {
        result = right * left;
      } else if (token == "/") {
        result = left / right;
      } else if (token == "^") {
        result = pow(left, right);
      }
      values.add(result);
    }
  }
  firstValue = values[0];
  return firstValue;
}

